# Description

This is the `Jupyter` notebook for identification of crosswalks on the area of ​​interest on provided `opendata` maps.

Tags: `Python`, `Jupyter Notebook`, `Geopandas`
## Sample 

![png](.img/output_4_1.png)
